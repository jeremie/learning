<?php
function DisplayTable($table)
{
	echo '<table>';
	foreach($table as $id => $row)
	{
		if ($id==0)
			{
				echo '<tr><td>id</td>';
				foreach($row as $cle => $element)
				{
					echo '<td>'.$cle . '</td>';
				}
				echo '</tr>';
			}
		echo '<tr><td>'.$id.'</td>';
		foreach($row as $cle => $element)
		{
			echo '<td>'.$element . '</td>';
		}
		echo '</tr>';
	}
	echo '</table>';
}
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Mon super site modifié</title>
    </head>
 
    <body>
 
    <!-- L'en-tête -->
    
    <header>
       
    </header>
    
    <!-- Le menu -->
    <?php include("menu.php"); ?>

    
    <!-- Le corps -->
    
    <div id="corps">
        <h1>Mon super site</h1>
        
        <p>
            Bienvenue sur mon super site !<br />
            Vous allez adorer ici, c'est un site génial qui va parler de... euh... Je cherche encore un peu le thème de mon site. :-D
        </p>
    </div>
    
    <!-- Le pied de page -->
    
    <footer id="pied_de_page">
        <p>Copyright moi, tous droits réservés</p>
    </footer>
    
    </body>
</html>

<?php
// On crée notre array $coordonnees
$coordonnees_1 = array (
    'prenom' => 'François',
    'nom' => 'Dupont',
    'adresse' => '3 Rue du Paradis',
    'ville' => 'Marseille');

$coordonnees_2 = array (
    'prenom' => 'Michel',
    'nom' => 'Martin',
    'adresse' => '10 Rue du Président',
    'ville' => 'Loriol');
	

$users = array ($coordonnees_1,$coordonnees_2);

DisplayTable($users);

if (array_key_exists('nom', $users[0]))
{
    echo 'La clé "nom" se trouve dans les coordonnées !</br>';
}

if (in_array('Dupont', $users[0]))
{
    echo 'La valeur "Dupont" se trouve dans le user 0 !</br>';
}

$position = array_search('Dupont', $users[0]);
echo '"Dupont" se trouve dans l attribut ' . $position . '<br />';
?>

<?php
$annee = date('Y');
echo $annee.'/';
$annee = date('m');
echo $annee.'/';
$annee = date('d');
echo $annee.' ';
$annee = date('H');
echo $annee.':';
$annee = date('i');
echo $annee.':';
$annee = date('s');
echo $annee;
?>
